Qbuild {
  maintainer "Sergey Gridassov", "grindars@gmail.com"
  homepage "http://www.alsa-project.org/"
  source "ftp://ftp.alsa-project.org/pub/lib/${P}.tar.bz2"
  license "GPL2"
  keywords 'arm', 'mips'
  can_use :lisp, :sequencer, :hwdep, :rawmidi, :pcm, :mixer, :aload

  prepare {
    qpatch 'mips-extern-fix-1.0.24.patch' if(_('${TARGET_ARCH}').include? 'mips')
    qpatch 'alsa-lib-configure.in.patch'
    qautoreconf
  }

  configure {
    qconfigure '--disable-python', enable(:lisp, "alisp"), '--disable-old-symbols', enable(:sequencer, "seq"), enable(:hwdep), enable(:rawmidi), enable(:pcm),
        enable(:mixer), enable(:aload)
  }

  package("libasound", "Shared library for ALSA applications", :libs, :optional) {
    depends 'libc'

    qinstall

    qdist 'usr/share/alsa', 'usr/lib/**/*.so*'
    qdist 'usr/bin/aserver' if uses? :pcm
  }

  package("libasound-dev", "Shared library for ALSA applications (development files)", :libs, :optional) {
    depends 'libasound'

    qinstall

    qdist 'usr/share/aclocal', 'usr/include', 'usr/lib/**/*.la', 'usr/lib/pkgconfig'
  }
}

