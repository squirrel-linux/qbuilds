/* uclibc doesn't have utmpx functions */

#include <utmpx.h>
#include <utmp.h>
#include <string.h>

struct utmpx *getutxent(void) {
	return (struct utmpx *) getutent();
}

struct utmpx *getutxid(const struct utmpx * utx) {
	return (struct utmpx *) getutid((struct utmp *) utx);
}

struct utmpx *getutxline(const struct utmpx * utx) {
	return (struct utmpx *) getutline((struct utmp *) utx);
}

struct utmpx *pututxline(const struct utmpx * utx) {
	return (struct utmpx *) pututline((struct utmp *) utx);
}

void setutxent(void) {
	setutent();
}

void endutxent(void) {
	endutent();
}

int utmpxname(const char *file) {
	return utmpname(file);
}

void getutmp(const struct utmpx *utmpx, struct utmp *utmp) {
	memcpy(utmp, utmpx, sizeof(utmp));
}

void getutmpx(const struct utmp *utmp, struct utmpx *utmpx) {
	memcpy(utmpx, utmp, sizeof(utmp));
}

void updwtmpx(const char *wtmpx_file, const struct utmpx *utmpx) {
	updwtmp(wtmpx_file, (const struct utmp *) utmpx);
}
