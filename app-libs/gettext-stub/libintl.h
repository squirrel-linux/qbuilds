#ifndef _LIBINTL_H
#define _LIBINTL_H 1

#include <features.h>

#if defined(__UCLIBC__) && !defined(__UCLIBC_HAS_GETTEXT_AWARENESS__)
/* Stubs for gettext/locale bullshit... */
#undef gettext
#undef dgettext
#undef dcgettext
#undef ngettext
#undef dngettext
#undef dcngettext
#undef textdomain
#undef bindtextdomain
#undef bind_textdomain_codeset
#undef _
#undef N_

static inline char *gettext(const char *__msgid) {
	return __msgid;
}

static inline char *dgettext(const char *__domain, const char *__msgid) {
	return __msgid;
}

static inline char *dcgettext(const char *__domainname,
                              const char *__msgid, int __category) {
	return __msgid;
}

static inline char *ngettext(const char *__msgid1, const char *__msgid2,
                             unsigned long int __n) {
	return ((__n) == 1 ? (const char *) (__msgid1) : (const char *) (__msgid2));
}

static inline char *dngettext(const char *__domainname, const char *__msgid1,
                              const char *__msgid2, unsigned long int __n) {
	return ((__n) == 1 ? (const char *) (__msgid1) : (const char *) (__msgid2));
}

static inline char *dcngettext(const char *__domainname, const char *__msgid1,
                               const char *__msgid2, unsigned long int __n,
                               int __category) {
	return ((__n) == 1 ? (const char *) (__msgid1) : (const char *) (__msgid2));
}

static inline char *bindtextdomain(const char *__domainname,
                                   const char *__dirname) {
	return __domainname;
}

static inline char *bind_textdomain_codeset(const char *__domainname,
                                            const char *__codeset) {
	return __codeset;
}

static inline char *textdomain(const char *__domainname) {
	return __domainname;
}

#define _(String) (String)
#define N_(String) (String)

#endif /* GETTEXT */
#endif /* _LIBINTL_H */
