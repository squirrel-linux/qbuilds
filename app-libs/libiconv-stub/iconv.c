#include <iconv.h>
#include <errno.h>

iconv_t iconv_open(const char *tocode, const char *fromcode) {
	errno = EINVAL;

	return (iconv_t) -1;
}

int iconv_close(iconv_t cd) {
	errno = EINVAL;

	return -1;
}

size_t iconv(iconv_t cd, char **inbuf, size_t *inbytesleft, char **outbuf, size_t *outbytesleft) {
	errno = EINVAL;

	return (size_t) -1;
}
