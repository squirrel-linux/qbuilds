Qbuild {
  maintainer "Peter Zotov", "whitequark@whitequark.org"
  homepage "http://gmplib.org/"
  license "LGPL3"
  source "ftp://ftp.gmplib.org/pub/${P}/${P}.tar.bz2"

  keywords 'tools'
  can_use :doc, :test # no :cxx as that would interfere with gcc building

  configure {
    qconfigure '--enable-cxx', 'CPPFLAGS=-fexceptions'
  }

  build {
    qmake
    qmake 'check' if can_exec? && uses?(:test)
  }

  package('libgmp3', "Multiprecision arithmetic library", :libs, :extra) {
    depends 'libc'

    qinstall
    qdist_lib 'gmp'
  }

  package('libgmp3-dev', "Developer files for libgmp3", :libs, :extra) {
    depends 'libgmp3'

    qinstall
    qdist_lib_dev 'gmp'
    qdist_doc
  }

  package('libgmpxx4', "Multiprecision arithmetic library - C++ binding", :libs, :extra) {
    depends 'libc'

    qinstall
    qdist_lib 'gmpxx'
  }

  package('libgmpxx4-dev', "Developer files for libgmpxx4", :libs, :extra) {
    depends 'libgmpxx4'

    qinstall
    qdist_lib_dev 'gmpxx'
  }
}
