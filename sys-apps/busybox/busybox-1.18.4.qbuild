Qbuild {
  maintainer "Peter Zotov", "whitequark@whitequark.org"
  homepage "http://busybox.net/"
  license "GPL2"
  source "http://www.busybox.net/downloads/${P}.tar.bz2"
  can_use :ipv6, :unicode, :rfkill, :systemd
  keywords 'arm', 'mips'

  override '.config', :as => 'busybox.conf', :or => 'config-1.18'

  tune {
    run "make menuconfig"
  }

  configure {
    sed "-e 's/@tramp@/#{_('${TRAMP}').gsub('/', '\/')}/'",
        "-e 's/@cross@/${CROSS}/'",
        "-e 's/@unicode@/#{y :unicode}/'",
        "-e 's/@rfkill@/#{y :rfkill}/'",
        "-i ${SRC}/.config"
    %w{FEATURE_IPV6 FEATURE_IFUPDOWN_IPV6
          PING6 TRACEROUTE6}.each { |v6f|
      echo "CONFIG_#{v6f}=#{y :ipv6}", '>>${SRC}/.config'
    }
  }

  package('busybox', "BusyBox combines tiny versions of many common UNIX utilities into a single small executable", :utils, :required) {
    qmake 'install'

    qdist 'bin', 'sbin', 'usr/bin', 'usr/sbin'
    if uses? :systemd
      mkdir '-p', '${PKG}/lib/systemd/system', '${PKG}/etc/systemd/system/multi-user.target.wants'
      ln '-s', '/lib/systemd/system/busybox-syslogd.service', '${PKG}/etc/systemd/system/multi-user.target.wants/busybox-syslogd.service'
      ln '-s', '/lib/systemd/system/busybox-klogd.service', '${PKG}/etc/systemd/system/multi-user.target.wants/busybox-klogd.service'
      cp '${FILES}/busybox-{syslogd,klogd}.service', '${PKG}/lib/systemd/system'
    else
      mkdir '-p', '${PKG}/etc/init/system'
      chmod 'u+s "${PKG}/bin/busybox"'
      cp '${FILES}/syslogd.conf', '${FILES}/klogd.conf', '${FILES}/getty.conf', '${FILES}/getty-spawner.conf', '${PKG}/etc/init/system/'
    end

    mkdir '-p ${PKG}/etc'
    cp '${FILES}/busybox.conf', '${PKG}/etc'

    mkdir '-p "${PKG}/usr/share/udhcpc"'
    cp '"${FILES}/default.script" "${PKG}/usr/share/udhcpc"'
    chmod '+x "${PKG}/usr/share/udhcpc/default.script"'
  }
}
