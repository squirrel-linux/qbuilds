Qbuild {
  maintainer "Peter Zotov", "whitequark@whitequark.org"
  homepage "http://ruby-lang.org"
  license "GPL2/Ruby"
  source "ftp://ftp.ruby-lang.org/pub/ruby/1.9/${P}.tar.gz"
  can_use :zlib, :curses, :readline, :openssl, :doc
  keywords 'mips'

  build_depends 'zlib' if uses? :zlib
  build_depends 'curses' if uses? :curses
  build_depends 'readline' if uses? :readline
  build_depends 'openssl' if uses? :openssl

  configure {
    qconfigure '--enable-pthread --disable-rpath ac_cv_func_setpgrp_void=yes',
               enable(:doc, 'install-doc'), '--program-suffix=1.9'
  }

  package('ruby1.9', "An interpreter of object-oriented scripting language Ruby", :runtime, :optional) {
    depends 'libc'
    depends 'zlib1' if uses? :zlib
    depends 'libcurses-0' if uses? :curses
    depends 'libreadline6' if uses? :readline

    qinstall_dostrip
    qdist_app
    qdist_doc
    qdist 'usr/lib/ruby/1.9.1'
  }

  package('ruby1.9-dev', "Development files for ruby", :runtime, :extra) {
    depends 'ruby1.9'

    qinstall_dostrip
    qdist 'usr/include', 'usr/lib/*.a'
  }
}
